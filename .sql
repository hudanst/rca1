IF NOT EXISTS(SELECT *
FROM sys.databases
WHERE NAME = 'coba')
BEGIN
    CREATE DATABASE coba
END

USE coba

IF NOT EXISTS(SELECT *
FROM INFORMATION_SCHEMA.TABLES
WHERE  TABLE_NAME = 'DataDiri')
BEGIN
    CREATE TABLE DataDiri
    (
        Id int unique not null,
        Nama varchar(100),
        Lokasi varchar(100),
        Umur int,
    )
END

INSERT INTO DataDiri
VALUES
    (1, 'A', 'Bekasi', 12),
    (2, 'A', 'Jakarta', 3),
    (3, 'A', 'Jogja', 17),
    (5, 'A', 'Kediri', 21),
    (11, 'A', 'Bekasi', 21),
    (12, 'A', 'Bekasi', 23),
    (13, 'A', 'Jakarta', 24),
    (20, 'A', 'Bekasi', 18),
    (40, 'A', 'Yogyakarta', 12),
    (41, 'A', 'Kediri', 12),
    (55, 'A', 'Laos', 40);



SELECT *
FROM DataDiri