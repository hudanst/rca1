const express = require('express')

const app = express()

// Add headers

const cros = () => {
    app.use((req, res, next) => {

        // Website you wish to allow to connect
        // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888')
        res.setHeader('Access-Control-Allow-Origin', '*')

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')

        // Request headers you wish to allow
        // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
        res.setHeader('Access-Control-Allow-Headers', 'x-auth-token,content-type')
        // res.setHeader('Access-Control-Allow-Headers', '*')

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true)//??

        // Pass to next layer of middleware
        next()
    })

}

module.exports = cros