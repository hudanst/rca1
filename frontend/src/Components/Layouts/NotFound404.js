import React from "react"

const NotFound404 = () => {
    const handleImgClick = () => {
        window.location.href = '/'
    }
    return (
        <center>
            <h1>
                Halaman tidak ditemukan 404
            </h1>
            <h2
                onClick={handleImgClick}
                style={{ color: 'lightblue', textDecoration: 'underline' }}
            >
                Click here to Home
            </h2>
        </center>
    )
}

export default NotFound404