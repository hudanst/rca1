import React from 'react'
import axios from 'axios'

import { IpAddres, tokenConfig } from '../../Function'

import Base from '../Containers/Base'

class Home extends React.Component {
    state = {
        Data: ''
    }
    async componentDidMount() {
        const Responses = await axios.get(`${IpAddres}/api/`, tokenConfig())
        //    console.log(Responses)
        if (Responses) {
            this.setState({ Data: Responses })
        }
    }
    render() {
        // console.log(this.state.Data)
        return (
            <Base Judul={'Home'} Data={this.state.Data} />
        )
    }
}

export default Home