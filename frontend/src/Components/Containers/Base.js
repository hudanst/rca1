import React from "react"

import Nav from './Nav'

import logo from '../../logo.svg'

import DataNotFound from './DataNotFound'

const Base = (props) => {
    // console.log(props.Data.data.data)
    const Data = props?.Data ? props.Data.data : ''
    // console.log(Data.data)
    return (
        <div className="App" >
            <header className="App-header">
                <Nav Lokasi={props?.Judul ? props.Judul : null} />
                <center>
                    <h1>
                        {props?.Judul ? props.Judul : 'Tidak ada judul'}
                    </h1>
                </center>
                <img src={logo} className="App-logo" alt="logo" />
                {props?.Data ? (
                    <table>
                        <thead>

                            <tr>
                                <th>index</th>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>Lokasi</th>
                                <th>Umur</th>
                            </tr>
                        </thead>
                        <tbody>

                            {Data.data.map((item, index) => (
                                <tr
                                    key={index}
                                >
                                    {/* {console.log(item.Lokasi)} */}
                                    <td>{index}</td>
                                    <td>{item.Id}</td>
                                    <td>{item.Nama}</td>
                                    <td>{item.Lokasi}</td>
                                    <td>{item.Umur}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                ) : (
                    <DataNotFound />
                )}
            </header>

        </div>
    )
}

export default Base