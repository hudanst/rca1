import React from "react"

import { Link as GoTo } from 'react-router-dom'

const Nav = (props) => {
    const defaultStyle = {
        border: '0px solid red', margin: ' 10px', color: 'lightblue', fontSize: '120%', textDecoration: 'none'
    }
    return (
        <div
            style={{ display: 'inline-flex' }}
        >
            <GoTo
                to='/'
                // id='FullNavbar_Blog'
                // onMouseOver={(e) => MouseHover(e)}
                // onMouseOut={(e) => MouseOut(e)}
                // style={{ ...Toolbar_Menu, marginLeft: '40%' }}
                style={{ ...defaultStyle, ...{ textDecoration: props?.Lokasi ? (props.Lokasi === 'Home' ? 'underline' : 'none') : 'none' } }}
            >
                Home
            </GoTo>
            <GoTo
                to='/sub'
                style={{ ...defaultStyle, ...{ textDecoration: props?.Lokasi ? (props.Lokasi === 'Sub' ? 'underline' : 'none') : 'none' } }}
            // id='FullNavbar_Blog'
            // onMouseOver={(e) => MouseHover(e)}
            // onMouseOut={(e) => MouseOut(e)}
            // style={{ ...Toolbar_Menu, marginLeft: '40%' }}
            >
                Sub
            </GoTo>
        </div >
    )
}

export default Nav