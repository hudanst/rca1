import React from "react"

const DataNotFound = () => {
    return (
        <h1>
            Data tidak ditemukan
        </h1>
    )
}

export default DataNotFound