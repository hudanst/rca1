import React from "react"

import { Routes ,Route } from 'react-router-dom'

import Home from './Components/Layouts/Home'
import Sub from './Components/Layouts/Sub'
import NotFound404 from './Components/Layouts/NotFound404'


const BaseRouter = ()=>{
    return(
        <Routes>
            <Route exact path="/" element={<Home/>} />
            <Route exact path="/sub" element={<Sub />} />
            <Route path="*" element={<NotFound404 />} />
        </Routes>
    )
}

export default BaseRouter