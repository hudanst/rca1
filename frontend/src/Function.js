import axios from 'axios'

// const Ip = '127.0.0.1'
const Ip = 'apietanacareer.azurewebsites.net'
// const Port = '5000'
const Port = null
// export const IpAddres = `http://${Ip}:${Port}`
export const IpAddres = `http://${Ip}${Port ? `:${Port}` : ''}`

export const tokenConfig = () => {
    // HEADERS
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return config
}

export const get_Data = () => async () => {
    try {
        const Responses = await axios.get(`${IpAddres}/api/`, tokenConfig())
        if (Responses) {
            return Responses
        }
    } catch (err) {
        console.log(err)
        return err
    }
}