import React, { Fragment } from 'react'

import { BrowserRouter as Router } from 'react-router-dom'

import MainRouter from './Router'

import './App.css'

const App = () => {
  return (
    <Fragment>
      <Router>
        <MainRouter />
      </Router>
    </Fragment>
  )
}

export default App

